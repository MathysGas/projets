from pile import *
import time

def mettre_disques(pile,n):
    for i in range (n,0,-1):
        pile.empiler(i)
        
        
def creation_tours(n):
    p0=Pile()
    p1=Pile()
    p2=Pile()
    mettre_disques(p0,n)
    return [p0,p1,p2]

def deplacer(tours,origine,cible):
    if not tours[origine].est_vide() and (tours[cible].est_vide() or tours[origine].consulter()<tours[cible].consulter()):
        tours[cible].empiler(tours[origine].depiler())
        

def resoudre(tours,n_disques,n,origine,cible,interm):
    if n==1:
        deplacer(tours,origine,cible)
        #affichage_tours(tours,n_disques)
    else:
        resoudre(tours,n_disques,n-1,origine,interm,cible)
        deplacer(tours,origine,cible)
        #affichage_tours(tours,n_disques)
        resoudre(tours,n_disques,n-1,interm,cible,origine)

    

def nb_etapes(n):
    if n==1:
        return 1
    else:
        return 2*nb_etapes(n-1)+1
    
def pile_vers_tableau(pile,n):
    tab=[0]*n
    tmp=Pile()
    while not pile.est_vide():
        tmp.empiler(pile.depiler())
    l=tmp.taille
    for i in range(l):
        a=tmp.depiler()
        tab[i]=a
        pile.empiler(a)
    return tab

def tours_vers_matrice(tours,n):
    return [pile_vers_tableau(tours[0],n),pile_vers_tableau(tours[1],n),pile_vers_tableau(tours[2],n)]

def affichage_matrice(matrice,n):
    for j in range(n-1,-1,-1): # parcours de droite à gauche des sous tableaux
        ligne=" "
        for i in range(3): # parcours de haut en bas des tableaux
            if matrice[i][j] == 0:
                ligne+= " | "
            else :
                ligne+=" "+str(matrice[i][j])+" "
        print(ligne)
    print("===========")
    
def affichage_tours(tours,n):
    matrice=tours_vers_matrice(tours,n)
    affichage_matrice(matrice,n)
    
    
        
        
tours=creation_tours(30)
#print(tours)
#deplacer(tours,0,1)
#print(pile_vers_tableau(tours[0],5))
#print(pile_vers_tableau(tours[1],5))
#print(tours_vers_matrice(tours,5))
#matrice=tours_vers_matrice(tours,5)
#affichage_tours(tours,50)
t1=time.time()
resoudre(tours,30,30,0,2,1)
t2=time.time()
print(t2-t1)
#print(tours)
#print(nb_etapes(100))