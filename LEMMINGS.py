class Case:
    def __init__(self,caractere):
        self.terrain=caractere
        self.lemming=None

    def __str__(self):
        if self.lemming!=None:
            return str(self.lemming)
        else:
            return self.terrain

    def libre(self):
        return self.lemming==None and self.terrain!="#"

    def depart(self):
        self.lemming=None

    def arrivee(self,lem):
        if self.terrain=="O":
            lem.sort()
        else:
            self.lemming=lem

class Jeu:
    def __init__(self,fichier):
        carte=open(fichier)
        tableau=[]
        for ligne in carte.readlines():
            l=[]
            for caractere in ligne:
                if caractere!="\n":
                    l.append(Case(caractere))
            tableau.append(l)

        self.grotte=tableau
        self.lemmings=[]
        self.lemmings_sauve = 0

    def affiche(self):
        for ligne in self.grotte:
            for case in ligne:
                print(case,end="")
            print()


    def tour(self):
        if self.lemmings!=[]:
            for lem in self.lemmings:
                lem.action()

    def demarre(self):
        while 1 :
            a=input("Pour inserer un lemming, tape 'l' ")
            if a == "l":
                lem=Lemming(self,0,1)
                self.lemmings.append(lem)
                self.grotte[0][1].arrivee(lem)
            elif a=="q":
                print("le nombre de lemmings sauvés est de ",str(self.lemmings_sauve))
                break
            else:
                self.tour()
            self.affiche()

        




class Lemming:
    def __init__(self,j,l,c):
        self.l=l
        self.c=c
        self.d=1
        self.j=j

    def __str__(self):
        if self.d==1:
            return ">"
        else:
            return "<"


    def action(self):
        if self.j.grotte[self.l + 1][self.c].libre():
            self.j.grotte[self.l ][self.c].depart()
            self.l+=1
            self.j.grotte[self.l][self.c].arrivee(self)
        elif self.j.grotte[self.l][self.c+self.d].libre():
            self.j.grotte[self.l][self.c].depart()
            self.c+=self.d
            self.j.grotte[self.l][self.c].arrivee(self)
        else:
            self.d=-self.d


    def sort(self):
        self.j.lemmings.remove(self)
        self.j.lemmings_sauve += 1


