import pygame
import sys
import random
import math

class Jeu:
    def __init__(self,mode):
        pygame.init()  # Initialize pygame
        self.ecran = pygame.display.set_mode((1200, 800))
        pygame.display.set_caption('Jeu Pong')
        self.jeu_encours = True
        self.jeu_termine=False
        self.joueur_1_x, self.joueur_1_y = 20, 250
        self.joueur_2_x, self.joueur_2_y = 1160, 250
        self.joueur_taille = [20, 150]
        self.vitesse_y_1, self.vitesse_y_2 = 0, 0
        self.joueur_1 = Joueur(self.joueur_1_x, self.joueur_1_y, self.joueur_taille)
        self.joueur_2 = Joueur(self.joueur_2_x, self.joueur_2_y, self.joueur_taille)
        self.rect = pygame.Rect(0, 0, 1200, 800)
        self.balle_direction = [-1, 1]
        self.balle = Balle(600, 400, [17, 17], random.choice(self.balle_direction))
        self.balle_tire = False
        self.balle_vitesse_x, self.balle_vitesse_y = 1,1
        self.score_joueur_1 = 0
        self.score_joueur_2 = 0
        self.mode=mode
    
    def boucle_principale(self):
        while not self.jeu_termine:
            if self.mode=="1v1":
                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        sys.exit()
                    if event.type == pygame.KEYDOWN:
                        if event.key == pygame.K_UP:
                            self.vitesse_y_1 = -2
                        if event.key == pygame.K_DOWN:
                            self.vitesse_y_1 = 2
                        if event.key == pygame.K_z:
                            self.vitesse_y_2 = -2
                        if event.key == pygame.K_s:
                            self.vitesse_y_2 = 2
                        if event.key == pygame.K_SPACE:
                            self.balle_tire = True
                        if event.key == pygame.K_a:
                            self.balle_vitesse_x, self.balle_vitesse_y = 5,5
                        if event.key == pygame.K_r:
                            self.balle_vitesse_x, self.balle_vitesse_y = 1,1
         
                            
                    if event.type == pygame.KEYUP:
                        if event.key == pygame.K_UP:
                            self.vitesse_y_1 = 0
                        if event.key == pygame.K_DOWN:
                            self.vitesse_y_1 = 0
                        if event.key == pygame.K_z:
                            self.vitesse_y_2 = 0
                        if event.key == pygame.K_s:
                            self.vitesse_y_2 = 0

                self.joueur_1.mouvement(self.vitesse_y_1)
                self.joueur_2.mouvement(self.vitesse_y_2)
                self.joueur_1.rect.clamp_ip(self.rect)
                self.joueur_2.rect.clamp_ip(self.rect)
                
                if not self.jeu_termine:
                    if self.balle_tire:
                        self.balle.mouvement(self.balle_vitesse_x, self.balle_vitesse_y)

                    if self.balle.rect.colliderect(self.joueur_1.rect):
                        self.balle_vitesse_x = 2(abs(self.balle_vitesse_x))

                    if self.balle.rect.colliderect(self.joueur_2.rect):
                        self.balle_vitesse_x = 2(abs(self.balle_vitesse_x))

                    if self.balle.rect.left < 0:
                        # Player 2 scores a point
                        self.score_joueur_2 += 1
                        self.reset_balle()

                    if self.balle.rect.right > 1200:
                        # Player 1 scores a point
                        self.score_joueur_1 += 1
                        self.reset_balle()

                    if self.balle.rect.top <= 0 or self.balle.rect.bottom >= 800:
                        self.balle_vitesse_y= -self.balle_vitesse_y
#                     

                if self.score_joueur_1 >= 11 or self.score_joueur_2 >= 11:
                    self.jeu_termine = True

                self.balle.rect.clamp_ip(self.rect)
                self.ecran.fill((50, 50, 50))
                self.balle.afficher(self.ecran)
                self.joueur_1.afficher(self.ecran)
                self.joueur_2.afficher(self.ecran)
                self.afficher_scores()
                pygame.display.flip()
                
                

            elif self.mode=="ia":
                
                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        sys.exit()
                    if event.type == pygame.KEYDOWN:
                        if event.key == pygame.K_UP:
                            self.vitesse_y_1 = -2
                        if event.key == pygame.K_DOWN:
                            self.vitesse_y_1 = 2
                        if event.key == pygame.K_SPACE:
                            self.balle_tire = True
         
                            
                    if event.type == pygame.KEYUP:
                        if event.key == pygame.K_UP:
                            self.vitesse_y_1 = 0
                        if event.key == pygame.K_DOWN:
                            self.vitesse_y_1 = 0
                


                        
            
                if self.balle.rect.x >200:
                    if self.balle.rect.y>400:
                        self.vitesse_y_2=1.3
                    elif self.balle.rect.y<400:
                        self.vitesse_y_2=-1.3
                        
                if self.balle.rect.x <=200:
                    if self.balle.rect.y>400:
                        self.vitesse_y_2=0
                    elif self.balle.rect.y<400:
                        self.vitesse_y_2=0

                    
                self.joueur_1.mouvement(self.vitesse_y_1)
                self.joueur_2.mouvement(self.vitesse_y_2)
                self.joueur_1.rect.clamp_ip(self.rect)
                self.joueur_2.rect.clamp_ip(self.rect)
                
                if not self.jeu_termine:
                    if self.balle_tire:
                        self.balle.mouvement(self.balle_vitesse_x, self.balle_vitesse_y)

                    if self.balle.rect.colliderect(self.joueur_1.rect):
                        self.balle_vitesse_x = -abs(self.balle_vitesse_x)

                    if self.balle.rect.colliderect(self.joueur_2.rect):
                        self.balle_vitesse_x = --abs(self.balle_vitesse_x)

                    if self.balle.rect.left < 0:
                        # Player 2 scores a point
                        self.score_joueur_2 += 1
                        self.reset_balle()

                    if self.balle.rect.right > 1200:
                        # Player 1 scores a point
                        self.score_joueur_1 += 1
                        self.reset_balle()

                    if self.balle.rect.top <= 0 or self.balle.rect.bottom >= 800:
                        self.balle_vitesse_y = -self.balle_vitesse_y

                if self.score_joueur_1 >= 11 or self.score_joueur_2 >= 11:
                    self.jeu_termine = True

                self.balle.rect.clamp_ip(self.rect)
                self.ecran.fill((50, 50, 50))
                self.balle.afficher(self.ecran)
                self.joueur_1.afficher(self.ecran)
                self.joueur_2.afficher(self.ecran)
                self.afficher_scores()
                pygame.display.flip()




        # Display the winner
        self.ecran.fill((50, 50, 50))
        self.afficher_scores()
        self.afficher_gagnant()
        pygame.display.flip()

        # Wait for a key press to reset the game
        attente = True
        while attente:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    sys.exit()
                if event.type == pygame.KEYDOWN and event.key == pygame.K_RETURN:
                    # Reset the game
                    self.jeu_termine = False
                    self.score_joueur_1 = 0
                    self.score_joueur_2 = 0
                    self.reset_balle()
                    attente = False
        
    def afficher_gagnant(self):
        font = pygame.font.Font(None, 72)
        if self.score_joueur_1 >= 11:
            text = font.render("Joueur 1 gagne!", True, (255, 255, 255))
        else:
            text = font.render("Joueur 2 gagne!", True, (255, 255, 255))

        text_rect = text.get_rect(center=(600, 400))
        self.ecran.blit(text, text_rect)
        
    def changement_direction_balle(self, vitesse, angle):
        vitesse = (vitesse * math.cos(angle))
        return vitesse

    def reset_balle(self):
        self.balle.rect.x = 600
        self.balle.rect.y = 400
        self.balle_tire = False

    def afficher_scores(self):
        font = pygame.font.Font(None, 36)
        score_text = font.render(f"Joueur 1: {self.score_joueur_1}  Joueur 2: {self.score_joueur_2}", True, (255, 255, 255))
        self.ecran.blit(score_text, (470, 10))


class Joueur:
    def __init__(self, x, y, taille):
        self.x = x
        self.y = y
        self.taille = taille
        self.rect = pygame.Rect(self.x, self.y, self.taille[0], self.taille[1])

    def mouvement(self, vitesse):
        self.rect.y += vitesse

    def afficher(self, surface):
        pygame.draw.rect(surface, (200, 200, 200), self.rect)


class Balle:
    def __init__(self, x, y, taille, direction):
        self.x = x
        self.y = y
        self.taille = taille
        self.direction = direction
        self.rect = pygame.Rect(self.x, self.y, self.taille[0], self.taille[1])
        self.vitesse_aleatoire_y = random.randint(1, 1)

    def mouvement(self, vitesse_x, vitesse_y):
        self.rect.x = (self.rect.x - self.direction * vitesse_x)
        self.rect.y += self.vitesse_aleatoire_y * vitesse_y

    def afficher(self, surface):
        pygame.draw.rect(surface, (230, 230, 230), self.rect)


# Create an instance of the Jeu class and run the game loop
jeu = Jeu("ia")
jeu.boucle_principale()


